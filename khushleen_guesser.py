possibilitites = [word.strip() for word in open("five_letter_words.txt")]

def possible_words(possibilities: list[str], prev_word: str, feedback: str) -> list[str]:
    GREEN, YELLOW, RED = "G", "Y", "R"
    remarks = list(enumerate(zip(prev_word, feedback)))
    
    for index, (letter, remark) in remarks:
        if remark == RED:
            possibilities = [word for word in possibilities if letter not in word]
        
        elif remark == GREEN:
            possibilities = [word for word in possibilities if word[index] == letter]
        
        else:
            possibilities = [word for word in possibilities if letter in word and word[index] != letter]

    return possibilities
