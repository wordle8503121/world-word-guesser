import random
import requests
from khushleen_guesser import possible_words
s = requests.Session()

#REGISTER
register_data = {
    "name": "wordlewizard"
}
url_register = "https://we6.talentsprint.com/game/register"
register_response = s.post(url_register, json = register_data)
register_response_json = register_response.json()



#CREATING A GAME
url_create = "https://we6.talentsprint.com/game/create"

create_data = {
    "id": register_response_json["id"],
    "overwrite": True
}
create_game_response = s.post(url_create, json = create_data)
create_game_response_json = create_game_response.json()



possibilities = [word.strip() for word in open("five_letter_word.txt")]


#FEEDBACK
def load_feedback(guess):
    guess_url = "https://we6.talentsprint.com/game/guess"

    guess_data = {
    "guess": guess,
    "id": register_response_json["id"]
    }

    guess_game_response = s.post(guess_url, json = guess_data)
    guess_game_response_json = guess_game_response.json()
    return guess_game_response_json["feedback"]


#GUESSING
prev_word = "earth"
feedback = load_feedback(prev_word)
print(f'{prev_word} with feedback {feedback}')

while (feedback != None) and (feedback != "GGGGG") and (possibilities != []):
    possibilities = possible_words(possibilities, prev_word, feedback)
    if possibilities != []:

        prev_word = random.choice(possibilities)
        feedback = load_feedback(prev_word)
        print(f'{prev_word} with feedback {feedback}')
    else:
        print("Sorry possibilities to test ended")


if feedback == "GGGGG":
    print(f'The word is {prev_word}')
    print("VICTORY!")
    

elif feedback == None:
    print("DEFEAT!")

elif possibilities == []:
    print("Possibilities to test ended")
