It is a python program to play WORDLE game using an API available at https://we6.talentsprint.com/apidocs 

It has three functional parts:

1. Sending a connection request to the API
    a. Generating id
    b. Creating game
    c. Start game

2. Read and write in JSON files

3. Program to filter 5 letter words as per the guesses to finally reach the correct word
