def filter_five_letter_words(input_file, output_file):
    
    with open(input_file) as file:
        words = file.read().split()
    
    five_letter_words = [word for word in words if len(word) == 5]
    
    with open(output_file, 'w') as file:
        file.write('\n'.join(five_letter_words))

# Example usage
input_file = 'sowpod.txt'
output_file = 'five_letter_word.txt'
filter_five_letter_words(input_file, output_file)

