possibilitites = [word.strip() for word in open("five_letter_word.txt")]

def possible_words(possibilities, prev_word, feedback):

    remarks = list(enumerate(zip(prev_word, feedback)))
    
    for index, (letter, remark) in remarks:
        if remark == "R":
            possibilities = [word for word in possibilities if letter not in word]
        
        elif remark == "G":
            possibilities = [word for word in possibilities if word[index] == letter]
        
        else:
            possibilities = [word for word in possibilities if letter in word and word[index] != letter]

    return possibilities
